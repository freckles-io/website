# Overview

This section contains details about all the *frecklets* that are shipped and useable with *freckles* by default.

## Available *frecklet* repositires

- [default](https://...)

## Access via cli

This list can also be queried and displayed via the commandline, either with:

```bash
freckfreckfreck tasks
```

or

```bash
frecklecute --help-all
```

Details can be displayed via:

```bash
freckfreckfreck task info <task_name> 
```

Use ``freckfreckfreck task --help`` to display information about how to only display certain aspects of a *frecklet* (e.g. only the vars).
