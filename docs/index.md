title: Home

# freckles <small> - DevOps for the rest of us</small>

*freckles* is a declarative, composable scripting environment that:

- helps you apply best practices *and* save time, short- as well as long-term
- has an as minimal as possible learning curve
- lets you create your own task-repository, and share it with your team or the wider world

## Features

### Minimal scripts

Have you ever installed a webserver by hand, and:

- set-up a letsencrypt certificate
- create a cron-job to renew said certificate
- redirected 'http' traffic to 'https'

... just to serve a simple static web-site? How long did that take you, including looking up blog posts and tutorials, copy-and-pasting code pieces you did not understand,
and general tinkering?

With *freckles*, your deploy script would look like:
``` yaml
# save in ~/.config/freckles/frecklets/deploy-hello-world.frecklet
- setup-webserver:                  # by default, nginx is used
    domain: dev.frkl.io
    use_https: true
    email: info@frkl.io             # used for letsencrypt cert request
    document_root: /var/www/html
- ensure-file-content:
    owner: www-data
    path: /var/www/html/index.html
    content: |
      <h1><i>freckles</i> says "hello", World!</h1>
```
That's all. After we mkae sure we have ssh-access to our server, we deploy:

``` bash
$ frecklecute -h root@dev.frkl.io deploy-hello-world
```

### Composable

Want to secure that machine? Sure!

``` yaml
# save in ~/.config/freckles/frecklets/secure-and-deploy.frecklet
- setup-basic-security:           # task description included with freckles
    ufw: true
    ufw_open_tcp:
      - 80
      - 443
    fail2ban: true
- deploy-hello-world
```

And kick it off with:

``` bash
$ frecklecute -h root@dev.frkl.io secure-and-deploy
```

### Integrated help

What exactly does the ``setup-basic-security`` task from above do? Let's ask:

```
$ freckfreckfreck task info -v setup-basic-security

setup-basic-security

  Basic security setup for a new server.

  Variables

    fail2ban
      desc: whether to install and enable fail2ban
      type: boolean
      required: False
      default: True

    ufw
      desc: whether to install and enable the ufw firewall
      type: boolean
      required: False
      default: True

    ufw_open_tcp
      desc: a list of tcp ports to open (if ufw enabled)
      type: list
      required: False
      default: -

    ufw_open_udp
      desc: a list of udp ports to open (if ufw enabled)
      type: list
      required: False
      default: -

  Description

    This frecklet can be used to harden a freshly installed server. It
    installs and configures the fail2ban [0] and ufw [1] packages.

  References:

    [0]: <www.fail2ban.org>
    [1]: <http://gufw.org/>

```

Right, and what sort of tasks are available? Also easy:

```
$ freckfreckfreck tasks

configure-service                 configure an init service
create-admin-user                 creating admin user
create-file                       ensures a file exists
create-folder                     ensures a folder exists
create-group                      ensures a group exists
create-parent-folder              ensures the parent folder of a path exists
create-user                       ensures a user exists on a system
deploy-hello-world                n/a
disable-service                   disable an init service
download-file                     downloads a file
...
...
```

### Use your task-lists as fully-featured command-line scripts

Using [Jinja2](http://jinja.pocoo.org/docs/2.10/) templating, your task-list files can grow into fully-fleged command-line applications. Let's make the domain name, and our website content value configurable:

``` yaml
# edit ~/.config/freckles/frecklets/deploy-hello-world.frecklet
- setup-webserver:                  # by default, nginx is used
    domain: "{{:: domain ::}}"
    use_https: true
    email: info@frkl.io             # used for letsencrypt cert request
    document_root: /var/www/html
- ensure-file-content:
    owner: www-data
    path: /var/www/html/index.html
    content: "{{:: content ::}}"
```

Here's how our script presents itself when we use the ``--help`` option:

```
$ frecklecute deploy-hello-world --help
Usage: frecklecute deploy-hello-world [OPTIONS]

  n/a

Options:
  --content TEXT  n/a  [required]
  --domain TEXT   n/a  [required]
  --help          Show this message and exit.
```

We can make this a bit prettier too:

``` yaml
doc:
  short_help: sets up a single-file static web-site
  help:
    Sets up a single-file static web-site.

    Installs the Nginx webserver, configures it for https and serves
    the content that was supplied to this script.

args:
  domain:
    doc:
      short_help: the domain name
    type: string
    required: false
    default: localhost
  content:
    doc:
      short_help: "the website content (in html)"
    type: string
    required: true

frecklets:
- setup-webserver:                  # by default, nginx is used
    domain: "{{:: domain ::}}"
    use_https: true
    email: info@frkl.io             # used for letsencrypt cert request
    document_root: /var/www/html
- ensure-file-content:
    owner: www-data
    path: /var/www/html/index.html
    content: "{{:: content ::}}"
```

And, this is what we get:

```
$ frecklecute deploy-hello-world --help
Usage: frecklecute deploy-hello-world [OPTIONS]

  Sets up a single-file static web-site. Installs the Nginx webserver,
  configures it for https and serves the content that was supplied to
  this script.

Options:
  --content TEXT  the website content (in html)  [required]
  --domain TEXT   the domain name
  --help          Show this message and exit.
```

We'd run this like:

```
$ frecklecute -h root@dev.frkl.io deploy-hello-world --domain dev.frkl.io --content "<h1>Hello, this is me talking! Can you hear me?</h1>
```

### ... and quite a bit more

For more information on other features, check out the [Documentation](/Documentation).

## Licensing

*freckles* is licensed under [The Parity Public License 3.0.0](https://gitlab.com/freckles-io/freckles/raw/develop/LICENSE). For details about what that means and information on when/how to purchase a private license, please check out the [Licensing page](/license).
